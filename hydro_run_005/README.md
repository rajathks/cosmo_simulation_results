# General Info about hydro_run_005 #

## output_00006 ##

* ncpu        =        200
* ndim        =          3
* levelmin    =          8
* levelmax    =         16
* ngridmax    =    4000000
* nstep_coarse=       1207

* boxlen      =  0.100000000000000E+01
* time        = -0.513581772709261E+01
* aexp        =  0.150308146542217E+00
* H0          =  0.678000030517578E+02
* omega_m     =  0.316000014543533E+00
* omega_l     =  0.684000015258789E+00
* omega_k     = -0.298023223876953E-07
* omega_b     =  0.450000017881393E-01
* unit_l      =  0.682815759510678E+24
* unit_d      =  0.804186577518120E-27
* unit_t      =  0.102632768041479E+17

* Total number of particles: 16777569
* Total number of dark matter particles: 16777216
* Total number of star particles: 353
* Total number of sink particles: 0
* Particle fields: pos vel mass iord level tform metal 

* nvar        =          7
* variable # 1: density
* variable # 2: velocity_x
* variable # 3: velocity_y
* variable # 4: velocity_z
* variable # 5: thermal_pressure
* variable # 6: passive_scalar_1
* variable # 7: passive_scalar_2

