# General Info about hydro_run_001 #

## output_00108 ##

* ncpu        =        100
* ndim        =          3
* levelmin    =          8
* levelmax    =         16
* ngridmax    =    4000000
* nstep_coarse=        107
 
* boxlen      =  0.100000000000000E+01
* time        = -0.122085013955358E+02
* aexp        =  0.479017438634751E-01
* H0          =  0.678000030517578E+02
* omega_m     =  0.316000014543533E+00
* omega_l     =  0.684000015258789E+00
* omega_k     = -0.298023223876953E-07
* omega_b     =  0.450000017881393E-01
* unit_l      =  0.217606739025539E+24
* unit_d      =  0.248456524642069E-25
* unit_t      =  0.104237419507249E+16

* Total number of particles: 16777216
* Total number of dark matter particles: 16777216
* Total number of star particles: 0
* Total number of sink particles: 0
* Particle fields: pos vel mass iord level tform metal 

* nvar        =          7
* variable # 1: density
* variable # 2: velocity_x
* variable # 3: velocity_y
* variable # 4: velocity_z
* variable # 5: thermal_pressure
* variable # 6: passive_scalar_1
* variable # 7: passive_scalar_2
