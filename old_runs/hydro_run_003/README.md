# General Info about hydro_run_003 #

## output_00005 ##

* ncpu        =        100
* ndim        =          3
* levelmin    =          8
* levelmax    =         16
* ngridmax    =    4000000
* nstep_coarse=        754

* boxlen      =  0.100000000000000E+01
* time        = -0.775300814423560E+01
* aexp        =  0.909264991586856E-01
* H0          =  0.678000030517578E+02
* omega_m     =  0.316000014543533E+00
* omega_l     =  0.684000015258789E+00
* omega_k     = -0.298023223876953E-07
* omega_b     =  0.450000017881393E-01
* unit_l      =  0.413058427044384E+24
* unit_d      =  0.363272872434283E-26
* unit_t      =  0.375579555479732E+16


* Total number of particles: 16777447
* Total number of dark matter particles: 16777216
* Total number of star particles: 231
* Total number of sink particles: 0
* Particle fields: pos vel mass iord level tform metal 

* nvar        =          7
* variable # 1: density
* variable # 2: velocity_x
* variable # 3: velocity_y
* variable # 4: velocity_z
* variable # 5: thermal_pressure
* variable # 6: passive_scalar_1
* variable # 7: passive_scalar_2

