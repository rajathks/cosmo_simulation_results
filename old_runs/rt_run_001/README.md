# General Info about rt_run_001 #

## output_00005 ##

* ncpu        =        100
* ndim        =          3
* levelmin    =          8
* levelmax    =         16
* ngridmax    =    4000000
* nstep_coarse=        143

* boxlen      =  0.100000000000000E+01
* time        = -0.101793789214970E+02
* aexp        =  0.625435718695922E-01
* H0          =  0.678000030517578E+02
* omega_m     =  0.316000014543533E+00
* omega_l     =  0.684000015258789E+00
* omega_k     = -0.298023223876953E-07
* omega_b     =  0.450000017881393E-01
* unit_l      =  0.284121237012603E+24
* unit_d      =  0.111623680475938E-25
* unit_t      =  0.177699564526564E+16

* Total number of particles: 16777223
* Total number of dark matter particles: 16777216
* Total number of star particles: 7
* Total number of sink particles: 0
* Particle fields: pos vel mass iord level tform metal 

* nvar        =         10
* variable # 1: density
* variable # 2: velocity_x
* variable # 3: velocity_y
* variable # 4: velocity_z
* variable # 5: thermal_pressure
* variable # 6: passive_scalar_1
* variable # 7: passive_scalar_2
* variable # 8: passive_scalar_3
* variable # 9: passive_scalar_4
* variable #10: passive_scalar_5

* nRTvar      =         12
* nIons       =          3
* nGroups     =          3
* iIons       =          8

* X_fraction  =  0.760000000000000E+00
* Y_fraction  =  0.240000000000000E+00

* unit_np     =  0.408744535237777E+04
* unit_pf     =  0.653535664441867E+12
* rt_c_frac   =  0.100000000000000E-01

* n_star      =  0.100000000000000E+00
* T2_star     =  0.100000000000000E+05
* g_star      =  0.133333330000000E+01

 Photon group properties------------------------------ 
  groupL0  [eV] =      13.600      24.590      54.420
  groupL1  [eV] =      24.590      54.420       0.000
  spec2group    =           1           2           3
  ---Group 1
  egy      [eV] =   1.776E+01
  csn    [cm^2] =   3.509E-18   0.000E+00   0.000E+00
  cse    [cm^2] =   3.269E-18   0.000E+00   0.000E+00
  ---Group 2
  egy      [eV] =   3.269E+01
  csn    [cm^2] =   6.640E-19   4.977E-18   0.000E+00
  cse    [cm^2] =   6.039E-19   4.670E-18   0.000E+00
  ---Group 3
  egy      [eV] =   5.620E+01
  csn    [cm^2] =   1.130E-19   1.586E-18   1.469E-18
  cse    [cm^2] =   1.124E-19   1.580E-18   1.461E-18
 -------------------------------------------------------
