#!/bin/bash
#SBATCH -t 02:00:00
#SBATCH --ntasks=100
#SBATCH --mem-per-cpu=8192
#SBATCH --mail-user=rajathks@outlook.com
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END

. ~/.profile
module load intel/2015.0.3.032

mpirun -np $SLURM_NTASKS ../../compiled_codes/ramses3d_star_formation ../../ic_files/ics_ramses_star_formation/ramses.nml
